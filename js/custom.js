var ctx_menu = document.getElementById('ctx-menu');

window.oncontextmenu = function(event) {
	ctx_menu.style.display = 'block';
	ctx_menu.style.top = `${event.clientY}px`;
	ctx_menu.style.left = `${event.clientX}px`;

	return false;
}

window.onclick = function() {
	ctx_menu.style.display = 'none';
}